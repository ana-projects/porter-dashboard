/**
 * This function guarantees the page is ready and calls all the custom code.
 **/
$(function() {
	expandList();
	showOpenMenu();
	hideOpenMenu();
	showLogoutMenu();
	hideLogoutMenu();
	showDeleteMenu();
	showNewAdminWindow();
	hideNewAdminWindow();
	readURL();
	dragRows();
	handleInlineInputs();
	selectRestrictionsMenu();
	showListUsers();
	selectAccessProfile();
	initDateTimePicker();
	selectNoLimit();
	checkBoxChange();
	showAccessWindow();
	hideAccessWindow();
	increaseAccess();
	validateLogin();
});

/**
 * This function binds the click event for the login form.
 **/
function validateLogin() {

	$(".form__button").click(function(event){
		var username = $("#user").val();
		var password = $("#password").val();
		event.preventDefault();

		if (username && password) {
			window.location.href = '/home-byadmin';
		} else {
			$(".form__alert").addClass("form__alert--active");
		}
	});
}

/**
 * This function binds the click event for the expanded list of places.
 **/
function expandList() {
	// Check if the list is already expanded or not.
	$(".table__column-icon.icon.icon-expand").click(function(){
		var $tableRow = $(this).closest(".table__row");

		// If row is already expanded:
		if ($tableRow.hasClass('table__row--is-expanded')) {
			// Collapse this row.
			$tableRow.removeClass('table__row--is-expanded');
		}
		// If is not expanded:
		else {
			// Expand this row.
			$tableRow.addClass('table__row--is-expanded');
		}
	});
}

/**
 * This function binds the click event to show the open menu.
 **/
function showOpenMenu() {
	$(".menu__icon").click(function(){
		$(".menu__open").addClass('menu__open--active');
	});
}

/**
 * This function binds the click event to hide the open menu.
 **/
function hideOpenMenu() {
	$(".icon.icon-menu_active").click(function(){
		$(".menu__open").removeClass('menu__open--active');

		// If menu logout is still opened:
		if($(".menu__logout").hasClass('menu__logout--active')) {
			$(".menu__logout").removeClass('menu__logout--active');
		}
	});
}

/**
 * This function binds the click event to show the logout menu.
 **/
function showLogoutMenu() {
	$(".menu__open-link[href='#logout']").click(function(){
		$(".menu__logout").addClass('menu__logout--active');
	});
}

/**
 * This function binds the click event to hide the logout menu.
 **/
function hideLogoutMenu() {
	$(".menu__logout-button.menu__logout-button--inverse").click(function(){
		$(".menu__logout").removeClass('menu__logout--active');
	});
}

/**
 * This function binds the click event to hide and show the delete menu.
 **/
function showDeleteMenu() {
	// Check if the menu is already open or not.
	$(".page-header__button.page-header__button--delete").click(function(){
		// If menu is already open:
		if ($(this).hasClass('page-header__button--delete-active')) {
			// Hide this menu.
			$(this).removeClass('page-header__button--delete-active');
			$(".menu__delete").removeClass('menu__delete--active');
		}
		// If is not open:
		else {
			// Show this menu.
			$(this).addClass('page-header__button--delete-active');
			$(".menu__delete").addClass('menu__delete--active');
		}
	});
	// Hides delete menu when user clicks on "no" button.
	$(".menu__delete-button.menu__delete-button--inverse").click(function(){
		// If menu is already open:
		if ($(".page-header__button.page-header__button--delete").hasClass('page-header__button--delete-active')) {
			// Hide this menu.
			$(".page-header__button.page-header__button--delete").removeClass('page-header__button--delete-active');
			$(".menu__delete").removeClass('menu__delete--active');
		}
	});
}

/**
 * This function binds the click event to show the add new admin modal window.
 **/
function showNewAdminWindow() {
	$(".page-column__add-result").click(function(){
		$(".lightbox.lightbox--admin").addClass('lightbox--open');
	});
}

/**
 * This function binds the click event to hide the add new admin modal window.
 **/
function hideNewAdminWindow() {
	$(".new-admin__button").click(function(){
		// If user clicks in cancel or skip buttons:
		if ($(this).hasClass('new-admin__button--cancel') || $(this).hasClass('new-admin__button--skip')) {
			// Hide modal window.
			$(".lightbox.lightbox--admin.lightbox--open").removeClass('lightbox--open');
		}
	});
}

/**
 * This function binds the click event to upload an image to the profile.
 **/
function readURL(input) {

    if (input !== undefined && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.form__column.form__column--picture').css('background-image', 'url(' + e.target.result + ')').text('');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".form__column.form__column--picture-input").change(function(){
    readURL(this);
});

/**
 * This function binds the drag related events for the expanded list of places.
 **/
function dragRows() {
	var $draggedElement;

	// Drop zones:
	$(".table__row.table__row--bigfont").on('dragover', function(e) {
		$(this).addClass('table__row--dropzone').siblings().removeClass('table__row--dropzone');

		if (e.preventDefault) {
			e.preventDefault(); // Necessary. Allows us to drop.
		}

		e.originalEvent.dataTransfer.dropEffect = 'move';
		return false;
	});

	$(".table__row.table__row--bigfont").on('drop', function(event) {
		var $targetTable = $(this).find(".table__expanded"),
		$droppedElement;

		// Check if we're dropping into the same table:
		if ($targetTable.is($draggedElement.parent())) {
			$droppedElement = $draggedElement;
		}
		else {
			// Add the element to end of the list.
			$droppedElement = $draggedElement.appendTo($targetTable);

			// If previous sibling has light background, remove class - else add.
			if ($droppedElement.prev(".table__row.table__row--smallfont[draggable]").hasClass('table__row--light')) {
				$droppedElement.removeClass('table__row--light');
			}
			else {
				$droppedElement.addClass('table__row--light');
			}
		}

		// Apply common effects after dropping:
		$droppedElement.show().removeClass('table__row--hide-before').nextAll().toggleClass('table__row--light');
		$targetTable.closest(".table__row--dropzone").removeClass('table__row--dropzone');

		// Reset helper variables:
		$draggedElement = undefined;
		return false;
	});

	// Draggable elements:
	$(".table__row.table__row--smallfont[draggable]").on('dragstart', function(event){
		event.originalEvent.dataTransfer.effectAllowed = 'move';
		event.originalEvent.dataTransfer.setData('text/html', $(this)[0].outerHTML);
		$(this)
			.addClass('table__row--hide-before')
			.nextAll().toggleClass('table__row--light');

		$(this)
			.closest(".table__row.table__row--bigfont").addClass('table__row--dropzone');
	});

	$(".table__row.table__row--smallfont[draggable]").on('drag', function(event) {
		// Only set new values if they are undefined:
		$draggedElement = $draggedElement || $(this).hide();
		return false;
	});
}

/**
 * This function binds click events to handle inline forms in expanded tables.
 **/
function handleInlineInputs() {
	$(".table__column-icon.icon.icon-edit").click(function(){
		// Enables input on the same row
		var $input =
		$(this)
			.eq(0)
			.closest(".table__row")
			.children(".form")
			.addClass("form--active")
			.children(".form__input")
			.removeAttr('disabled')
			.focus();

		// Move cursor to the end of the value and bind focusout event.
		$input.val($input.val());
	});

	$(".form.form--places-list .form__button").click(function(event){
		event.preventDefault();
		$(this)
			.closest(".table__row")
			.children(".form")
			.removeClass("form--active")
			.children(".form__input")
			.attr('disabled', 'disabled')
	});
}

/**
 * This function binds the click event to select the users menu in access modal window.
 **/
function selectRestrictionsMenu() {
	$(".access__restrictions-menu-item").click(function(){
		// If menu is not selected:
		if (!$(this).hasClass('access__restrictions-menu-item--selected')) {
			// Removes highlight from all buttons.
			$(".access__restrictions-menu-item")
				.removeClass('access__restrictions-menu-item--selected');

			// Highlights menu item.
			$(this).addClass('access__restrictions-menu-item--selected');

			// Hides all tables.
			$(this)
				.closest(".access__restrictions-table")
				.children(".modal-table")
				.removeClass('modal-table--active');

			// If click on users menu item.
			if ($(this).hasClass('access__restrictions-menu-item--users')) {
				// Displays users table.
				$(this)
					.closest(".access__restrictions-table")
					.children(".modal-table--users")
					.addClass('modal-table--active');
			}
			else {
				// Displays restrictions table.
				$(this)
					.closest(".access__restrictions-table")
					.children(".modal-table--restrictions")
					.addClass('modal-table--active');
			}
		}
	});
}

/**
 * This function binds the click event to show the list of users in the access modal window.
 **/
function showListUsers() {
	$(".users__button").click(function(){
		// If user clicks in skip button:
		if ($(this).hasClass('users__button--skip')) {
			// Shows list of users.
			$(this)
				.closest('.users')
				.children(".users__list")
				.addClass('users__list--active');
			// Hides alerts.
			$(this)
				.closest('.users')
				.children(".users__action")
				.removeClass('users__action--active');
		}
	});
}

/**
 * This function binds the click event to select a profile in the access modal window.
 **/
function selectAccessProfile() {
	$(".access__list-form.access__list-form--edit").click(function(){
		// If profile is not selected:
		if ($(this).hasClass('access__list-form--disable')) {
			// Disables input in all profiles.
			$(".access__list-form.access__list-form--edit")
				.addClass('access__list-form--disable');
			// Enables input in selected profile.
			$(this)
				.removeClass('access__list-form--disable')
				.children(".form-input")
				.removeAttr('disabled');
		}
	});
}

/**
 * This function initializes the datetimepickers.
 **/
function initDateTimePicker() {
	moment.updateLocale('en-gb', {
		weekdaysMin : "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_")
	});
	$calendars = $(".form-input--calendar").datetimepicker({
		locale: 'en-gb',
		format: 'DD-MMM HH:mm',
		icons: {
			previous: 'icon icon-left',
			next: 'icon icon-right',
			up: 'icon icon-up',
			down: 'icon icon-down',
			time: 'icon icon-time',
			date: 'icon icon-calendar'
		}
	});
	$calendars.on('dp.show', function() {
		$calendar = $(this).siblings(".bootstrap-datetimepicker-widget");
		$calendar.find(".prev, .next").attr('colspan', '2');
		$calendar.find(".picker-switch").attr('colspan', '3');
		$calendar.find(".timepicker-hour").parent().prepend('<span class="timepicker-hour-tag">HOURS</span>');
		$calendar.find(".timepicker-minute").parent().prepend('<span class="timepicker-minute-tag">MINUTES</span>');
		$calendar.find(".month").closest("tr").prepend('<span class="month-tag">MONTHS</span>');
		$calendar.find(".year").closest("tr").prepend('<span class="year-tag">YEARS</span>');
		$calendar.find("[title='Select Decade']").removeAttr("data-action");

		$(this).css({
			'position': 'relative',
			'z-index': '2',
			'border': '2px solid #21FFD3'
		});

		if ($calendar.hasClass('top')) {
			$(this).css('border-top', 'none');
		}
		else {
			$(this).css('border-bottom', 'none');
		}

		// Disable NO LIMIT button.
		$(this).closest(".restrictions__row")
		.find(".restrictions__button-column .restrictions__button")
		.removeClass('restrictions__button--selected');

	}).on('dp.hide', function() {
		$(this).css({
			'position': 'static',
			'z-index': '0',
			'border': 'none'
		});
	});

	$schedulers = $(".form-input--schedule").datetimepicker({
		locale: 'en-gb',
		format: 'HH:mm',
		icons: {
			previous: 'icon icon-left',
			next: 'icon icon-right',
			up: 'icon icon-up',
			down: 'icon icon-down',
			time: 'icon icon-time',
			date: 'icon icon-calendar'
		}
	});
	$schedulers.on('dp.show', function() {
		$scheduler = $(this).siblings(".bootstrap-datetimepicker-widget");
		$scheduler.find(".timepicker-hour").parent().prepend('<span class="timepicker-hour-tag">HOURS</span>');
		$scheduler.find(".timepicker-minute").parent().prepend('<span class="timepicker-minute-tag">MINUTES</span>');

		$(this).css({
			'position': 'relative',
			'z-index': '2',
			'border': '2px solid #21FFD3'
		});

		if ($scheduler.hasClass('top')) {
			$(this).css('border-top', 'none');
		}
		else {
			$(this).css('border-bottom', 'none');
		}

		// Disable NO LIMIT button.
		$(this).closest(".restrictions__row")
		.find(".restrictions__button-column .restrictions__button")
		.removeClass('restrictions__button--selected');

	}).on('dp.hide', function() {
		$(this).css({
			'position': 'static',
			'z-index': '0',
			'border': 'none'
		});
	});
}

/**
 * This function binds the click event to select the button no limit.
 **/
function selectNoLimit() {
	$(".restrictions__button").click(function(){
		$(this).toggleClass('restrictions__button--selected');
		if ($(this).hasClass('restrictions__button--selected')) {
			$(this)
			.closest(".restrictions__row")
			.children(".restrictions__content")
			.children(".form-input").each(function() {
				if ($(this).attr('type') === 'checkbox') {
					$(this).prop('checked', '');
				}
				else if ($(this).data('DateTimePicker')) {
					$(this).data('DateTimePicker').clear();
				}
				else {
					$(this).val('');
				}
			});
		}
	});
}

/**
 * This function binds the change event to checkboxes to disable NO LIMIT.
 **/
function checkBoxChange() {
	$(".restrictions__content .form-input--checkbox").change(function(){
		// Disable NO LIMIT button.
		$(this).closest(".restrictions__row")
		.find(".restrictions__button-column .restrictions__button")
		.removeClass('restrictions__button--selected');
	});
}


/**
 * This function binds the click event to display the access window.
 **/
function showAccessWindow() {
	$(".table__column-icon.icon.icon-access").click(function(){
		$(this).closest("body").children(".lightbox").addClass('lightbox--open');
	});
}

/**
 * This function binds the click event to close the access window.
 **/
function hideAccessWindow() {
	$(".access__header-button.access__header__button--close").click(function(){
		$(this).closest(".lightbox.lightbox--open").removeClass('lightbox--open');
	});
}

/**
 * This function binds the click event to increment and decrement the number of accesses in the access window.
 **/
function increaseAccess() {
	$(".restrictions__button-counter").click(function() {
		var $button = $(this),
		oldValue = parseInt($button.closest(".restrictions__content").children(".form-input.form-input--number-accesses").val()) || 0,
		newValue = oldValue;

		if ($button.children().hasClass('icon-add')) {
			newValue = parseInt(oldValue) + 1;
		}
		else if (oldValue > 0) {
			newValue = parseInt(oldValue) - 1;
		}

		$button.closest(".restrictions__content").children(".form-input.form-input--number-accesses").val(newValue).trigger('change');
	});

	$(".form-input.form-input--number-accesses").change(function() {
		if ($(this).val() < 0) {
			$(this).val(0);
		}

		// Disable NO LIMIT button.
		$(this).closest(".restrictions__row")
		.find(".restrictions__button-column .restrictions__button")
		.removeClass('restrictions__button--selected');
	});
}
